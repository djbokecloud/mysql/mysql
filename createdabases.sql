/*
*data:2019-08-07
*/
DROP DATABASE IF EXISTS `djboke`;
set character_set_database = 'utf8';
set character_set_server = 'utf8';
set names 'utf8';
CREATE DATABASE `djboke`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';
COMMIT;
USE djboke; 

