/*
 Navicat Premium Data Transfer

 Source Server         : tenxunyun
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 212.64.29.29:3306
 Source Schema         : djboke

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/08/2019 16:44:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for boke_photo
-- ----------------------------
DROP TABLE IF EXISTS `boke_photo`;
CREATE TABLE `boke_photo`  (
  `photo_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '头像主键id',
  `photo_src` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '头像路劲',
  PRIMARY KEY (`photo_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '头像表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
