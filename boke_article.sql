/*
 Navicat Premium Data Transfer

 Source Server         : tenxunyun
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 212.64.29.29:3306
 Source Schema         : djboke

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/08/2019 15:22:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for boke_article
-- ----------------------------
DROP TABLE IF EXISTS `boke_article`;
CREATE TABLE `boke_article`  (
  `boke_article_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '博客id',
  `boke_article_startime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `boke_article_endtime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '发布时间',
  `boke_article_updatetime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `boke_article_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '博客内容',
  `boke_article_like` bigint(9) NULL DEFAULT NULL COMMENT '点赞数',
  `boke_article_image` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '博客书面图像',
  `boke_article_discuss` bigint(9) NULL DEFAULT NULL COMMENT '博客评论id',
  `boke_article_userId` bigint(9) NULL DEFAULT NULL COMMENT '用户id',
  `boke_article_status` int(2) NULL DEFAULT 0 COMMENT '博客文章状态(0未发布,1已发布)',
  PRIMARY KEY (`boke_article_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
