/*
 Navicat Premium Data Transfer

 Source Server         : tenxunyun
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 212.64.29.29:3306
 Source Schema         : djboke

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/08/2019 15:23:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for leave_word
-- ----------------------------
DROP TABLE IF EXISTS `leave_word`;
CREATE TABLE `leave_word`  (
  `leave_word_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `leave_word_text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `leave_word_data` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `boke_user_id` int(9) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`leave_word_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客留言表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
