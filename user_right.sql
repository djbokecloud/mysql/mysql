/*
 Navicat Premium Data Transfer

 Source Server         : tenxunyun
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 212.64.29.29:3306
 Source Schema         : djboke

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/08/2019 16:44:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_right
-- ----------------------------
DROP TABLE IF EXISTS `user_right`;
CREATE TABLE `user_right`  (
  `user_right_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `user_id` bigint(9) NOT NULL COMMENT '用户id',
  `right_id` bigint(9) NOT NULL COMMENT '权限id',
  PRIMARY KEY (`user_right_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户权限中间表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
