/*
 Navicat Premium Data Transfer

 Source Server         : tenxunyun
 Source Server Type    : MySQL
 Source Server Version : 50727
 Source Host           : 212.64.29.29:3306
 Source Schema         : djboke

 Target Server Type    : MySQL
 Target Server Version : 50727
 File Encoding         : 65001

 Date: 08/08/2019 15:22:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for boke_user
-- ----------------------------
DROP TABLE IF EXISTS `boke_user`;
CREATE TABLE `boke_user`  (
  `boke_user_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `boke_uname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `boke_user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '登陆密码',
  `boke_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '博客名',
  `boke_user_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `boke_user_phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电话',
  `boke_user_qq` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'QQ',
  `boke_user_createdata` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `boke_user_updatatime` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '修改时间',
  `boke_user_photo` bigint(9) NULL DEFAULT NULL COMMENT '头像id',
  `boke_lognumber` int(9) NULL DEFAULT NULL COMMENT '登陆次数',
  PRIMARY KEY (`boke_user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '博客用户表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
